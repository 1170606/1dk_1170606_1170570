package com.mycompany.Modelo;

import java.util.ArrayList;

public class RegistoCartoes {
    
    ArrayList<Cartao> cartoes;

    /**
     *
     * @param IDCartao
     * @return 
     */
    public Cartao getCartao(String IDCartao) {
        for (Cartao c : cartoes) {
            if (c.getIdCartao().equalsIgnoreCase(IDCartao)) {
                return c;
            }
        }
        return null;
    }

}
