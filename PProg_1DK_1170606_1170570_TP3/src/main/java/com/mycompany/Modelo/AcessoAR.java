package com.mycompany.Modelo;

import Utils.*;

public class AcessoAR {

	private Data data;
	private Hora hora;
	private int mov;
	private boolean sucesso;
	private Colaborador col;
        private Cartao cartao;
        private Equipamento eq;

    public AcessoAR() {
    }

    public AcessoAR(Data data, Hora hora, int mov, boolean sucesso, Colaborador col, Cartao cartao, Equipamento eq) {
        this.data = data;
        this.hora = hora;
        this.mov = mov;
        this.sucesso = sucesso;
        this.col = col;
        this.cartao = cartao;
        this.eq = eq;
    }

    /**
     * @return the sucesso
     */
    public boolean isSucesso() {
        return sucesso;
    }

    /**
     * @param sucesso the sucesso to set
     */
    public final void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }
        
}