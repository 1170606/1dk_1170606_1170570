/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Modelo;

import Utils.Data;

/**
 *
 * @author Utilizador
 */
public class Cartao {
    
    String idCartao;
    String dataEmissao;
    AtribuicaoCartao atrCartao;
    
    
        public Cartao(String idCartao, String dataEmissao, AtribuicaoCartao atrCartao) {
        this.idCartao = idCartao;
        this.dataEmissao = dataEmissao;
        this.atrCartao = atrCartao;
    }
    
    public void setIdCartao(String idCartao) {
        this.idCartao = idCartao;
    }

    public void setDataEmissao(String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public void setAtrCartao(AtribuicaoCartao atrCartao) {
        this.atrCartao = atrCartao;
    }


    
    
    public Colaborador getColaboradorAtribuido(Data data){
        return atrCartao.getCo();
    }

    public Cartao(String idCartao) {
        this.idCartao = idCartao;
    }

    public String getIdCartao() {
        return idCartao;
    }
    
    
    
}
