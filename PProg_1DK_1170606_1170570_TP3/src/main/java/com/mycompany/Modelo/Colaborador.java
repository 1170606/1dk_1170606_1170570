package com.mycompany.Modelo;

public class Colaborador {

    private int numMecanografico;
    private String nomeCompleto;
    private String nomeAbreviado;
    private PerfilA perfil;

    public Colaborador(int numMecanografico, String nomeCompleto, String nomeAbreviado) {
        this.numMecanografico = numMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
    }

    public Colaborador(Colaborador oCo) {
        this.numMecanografico = numMecanografico;
        this.nomeCompleto = nomeCompleto;
        this.nomeAbreviado = nomeAbreviado;
    }

    /**
     * @return the numMecanografico
     */
    public int getNumMecanografico() {
        return numMecanografico;
    }

    /**
     * @param numMecanografico the numMecanografico to set
     */
    public void setNumMecanografico(int numMecanografico) {
        this.numMecanografico = numMecanografico;
    }

    /**
     * @return the nomeCompleto
     */
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    /**
     * @param nomeCompleto the nomeCompleto to set
     */
    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    /**
     * @return the nomeAbreviado
     */
    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    /**
     * @param nomeAbreviado the nomeAbreviado to set
     */
    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    public PerfilA getPerfil() {
        return perfil;
    }

}
