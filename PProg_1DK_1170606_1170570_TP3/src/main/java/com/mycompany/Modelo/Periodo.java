/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Modelo;

import Utils.Data;
import Utils.Hora;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Utilizador
 */
public class Periodo implements Serializable{
    private String diaDaSemana;
    private Hora horaInicio;
    private Hora horaSaida;
    private Equipamento equipamento;

    public Periodo(String diaDaSemana, Hora horaInicio, Hora horaFim, Equipamento e) {
        this.diaDaSemana = diaDaSemana;
        this.horaInicio = horaInicio;
        this.horaSaida = horaFim;
        this.equipamento = e;
    }

    public Periodo() {
    }
    
    public Periodo(Periodo oPe){
        setDiaDaSemana(oPe.diaDaSemana);
        setHoraInicio(oPe.horaInicio);
        setHoraSaída(oPe.horaSaida);
        setE(oPe.equipamento);
    }
    /**
     * @return the diaDaSemana
     */
    public String getDiaDaSemana() {
        return diaDaSemana;
    }

    /**
     * @param diaDaSemana the diaDaSemana to set
     */
    public final void setDiaDaSemana(String diaDaSemana) {
        this.diaDaSemana = diaDaSemana;
    }

    /**
     * @return the horaInicio
     */
    public Hora getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param horaInicio the horaInicio to set
     */
    public void setHoraInicio(Hora horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * @return the horaFim
     */
    public Hora getHoraSaída() {
        return horaSaida;
    }

    /**
     * @param horaFim the horaFim to set
     */
    public final void setHoraSaída(Hora horaFim) {
        this.horaSaida = horaFim;
    }

    /**
     * @return the e
     */
    public Equipamento getE() {
        return equipamento;
    }

    /**
     * @param e the e to set
     */
    public final void setE(Equipamento e) {
        this.equipamento = e;
    }

    @Override
    public String toString() {
        return "Dia da Semana -> " + diaDaSemana + "\nHora de início -> " + horaInicio + "\nHora de Saída -> " + horaSaida + "\nEquipamento -> " + equipamento;
    }
    
    
}