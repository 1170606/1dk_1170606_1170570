package com.mycompany.Modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class RegistoPerfilA implements Serializable {

    private ArrayList<PerfilA> perfis;
    private PerfilA p;

    public RegistoPerfilA() {
        perfis = new ArrayList<>();
    }

    public RegistoPerfilA(ArrayList<PerfilA> lista) {
        this.setLista(lista);
    }

    /*//
        public void novoPerfil() {
            
	} */
//        
//        public boolean addPerfil(String id, String descricao){
//            for(PerfilA p : lista){
//                if(p.getId().equals(id)){
//                    return false;
//                }
//            }
//               return lista.add(new PerfilA(id, descricao));
//            
//        }
    /**
     *
     * @param p
     */
    public boolean registaPerfil(PerfilA p) {
        return perfis.add(p);
    }

    /**
     *
     * @param p
     */
    private void add(PerfilA p) {

        perfis.add(p);
    }

    public ArrayList<PerfilA> getLista() {
        return perfis;
    }

    public final void setLista(ArrayList<PerfilA> lista) {
        this.perfis = new ArrayList<>(lista);
    }

    public PerfilA novoPerfilDeAutorizacao() {
        return (new PerfilA());
    }

}
