package com.mycompany.Modelo;

public class Empresa {

    private RegistoPerfilA rp;
    private RegistoCartoes rc;
    private RegistoEquipamentos re;
    private RegistoAcessos ra;

    public Empresa() {
        this.rp = new RegistoPerfilA();
        this.rc = new RegistoCartoes();
        this.re = new RegistoEquipamentos();
        this.ra = new RegistoAcessos();
        
    }

    public RegistoPerfilA getRegistoPerfil() {
        return rp;

    }

    public RegistoEquipamentos getRegistoEquipamentos() {
        return re;
    }

    public RegistoAcessos getRegistoAcessos() {
        return ra;
    }

    public RegistoCartoes getRc() {
        return rc;
    }
    
    

}
