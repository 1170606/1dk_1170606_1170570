package com.mycompany.Modelo;

import Utils.*;
import java.util.ArrayList;

public class RegistoAcessos {

    ArrayList<AcessoAR> acessos;

    /**
     *
     * @param e
     * @param cart
     * @param colab
     * @param data
     * @param hora
     * @param sucesso
     * @param mov
     */
    public AcessoAR novoAcesso(Equipamento e, Cartao cart, Colaborador colab, Data data, Hora hora, boolean sucesso, int mov) {
        acessos = new ArrayList<>();
        AcessoAR acesso = new AcessoAR(data, hora, mov, sucesso, colab, cart, e);
        acessos.add(acesso);
        return acesso;
    }

    public ArrayList<AcessoAR> getAcessos() {
        return acessos;
    }

}
