package com.mycompany.Modelo;

import java.util.ArrayList;

public class RegistoEquipamentos {

    /**
     *
     * @param IDEquipamento
     */
    ArrayList<Equipamento> equipamentos;

    public RegistoEquipamentos() {
        equipamentos = new ArrayList<>();
    }

    public Equipamento getEquipamentoByID(String IDEquipamento) {
        for (Equipamento e : equipamentos) {
            if (e.getEndLogico().equalsIgnoreCase(IDEquipamento)) {
                return e;
            }
        }
        return null;
    }

    public ArrayList<Equipamento> getListaEquipamentos() {

        return equipamentos;
    }

    public void addEquipamentos(Equipamento e) {
        equipamentos.add(e);
    }

}
