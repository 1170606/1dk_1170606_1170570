package com.mycompany.Modelo;

import java.io.Serializable;

public class Equipamento implements Serializable {

    private String endLogico;
    private String endFisico;
    private int mov;
    private AreaRestrita areaRestrita;

    public Equipamento(String endLogico) {
        this.endLogico = endLogico;
    }

    public Equipamento(String endLogico, String endFisico, int mov, AreaRestrita areaRestrita) {
        this.endFisico = endFisico;
        this.endLogico = endLogico;
        this.mov = mov;
        this.areaRestrita = areaRestrita;
    }

    public Equipamento(Equipamento oEq) {
        setEndFisico(oEq.endFisico);
        setEndLogico(oEq.endLogico);
        setMov(oEq.mov);
        setAreaRestrita(oEq.areaRestrita);
    }

    public Equipamento() {
        this.areaRestrita = new AreaRestrita(mov);
    }

    public AreaRestrita getAreaRestrita() {
        return areaRestrita;
    }

    public int getMovimento() {
        return mov;
    }

    /**
     * @return the endLogico
     */
    public String getEndLogico() {
        return endLogico;
    }

    /**
     * @param endLogico the endLogico to set
     */
    public final void setEndLogico(String endLogico) {
        this.endLogico = endLogico;
    }

    /**
     * @return the endFisico
     */
    public String getEndFisico() {
        return endFisico;
    }

    /**
     * @param endFisico the endFisico to set
     */
    public final void setEndFisico(String endFisico) {
        this.endFisico = endFisico;
    }

    /**
     * @return the mov
     */
    public int getMov() {
        return mov;
    }

    /**
     * @param mov the mov to set
     */
    public final void setMov(int mov) {
        this.mov = mov;
    }

    public final void setAreaRestrita(AreaRestrita areaRestrita) {
        this.areaRestrita = areaRestrita;
    }

    @Override
    public String toString() {
        return endLogico;
    }

}
