package com.mycompany.Modelo;

import Utils.*;
import java.util.ArrayList;

public class PerfilA {

    private String id;
    private String descricao;
    private ArrayList<Periodo> periodos;

    /**
     *
     * @param e
     * @param data
     * @param hora
     */
    public boolean isEquipamentoAutorizado(Equipamento e, Data data, Hora hora) {
        Periodo per = null;
        for(Periodo p : periodos){
            if(p.getDiaDaSemana().equals(data.determinarDiaDaSemana()) && p.getHoraInicio().isMenor(hora) && p.getHoraSaída().isMaior(hora) && p.getE().equals(e)){
                per = p;
            }
        }
        return per != null;
    }

    public PerfilA() {
        periodos = new ArrayList<>();
    }

    public PerfilA(String id, String descricao) {
        periodos = new ArrayList<>();
        this.id = id;
        this.descricao = descricao;
    }

    public PerfilA(PerfilA oP) {
        setId(oP.id);
        setDescricao(oP.descricao);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(String id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public final void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPeriodos(ArrayList<Periodo> periodos) {
        this.periodos = periodos;
    }


}
