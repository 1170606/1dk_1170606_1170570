package com.mycompany.Modelo;

public class AreaRestrita {

    private int lotacao;

    public AreaRestrita(int lotacao) {
        this.lotacao = lotacao;
    }

    /**
     *
     * @param mov
     */
    public boolean lotacaoPermiteAcesso(int mov) {
        return lotacao >= mov;
    }

}
