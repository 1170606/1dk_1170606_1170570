/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.UI;

import Controller.RegistarPerfilController;
import Utils.Hora;
import com.mycompany.Modelo.Empresa;
import com.mycompany.Modelo.Equipamento;
import com.mycompany.Modelo.Periodo;
import com.mycompany.Modelo.RegistoEquipamentos;
import com.mycompany.pprog_1dk_1170606_1170570.MainApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author guial
 */
public class RegistoPerfil implements Initializable {

    private Alert alerta;
    @FXML
    private TextField txf_id;
    @FXML
    private TextField txf_descricao;
    @FXML
    private ComboBox<String> cb_equipamentos;
    @FXML
    private TextField txf_MinutosFIm;
    @FXML
    private TextField txf_HorasFim;
    @FXML
    private TextField txf_horaInicio;
    @FXML
    private TextField txf_MinutosInicio;
    @FXML
    private Button btt_adicionar;
    @FXML
    private ComboBox<String> cb_DiaSemana;
    @FXML
    private Button btt_apagar;
    @FXML
    private Button btt_registar;
    @FXML
    private Button btt_cancelar;
    @FXML
    private ListView<Periodo> listaPeriodos;

    private RegistarPerfilController controller;
    private RegistoEquipamentos registoEquipamentos;
    private MainApp ma;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Empresa emp = new Empresa();
        registoEquipamentos = new RegistoEquipamentos();
        controller = new RegistarPerfilController(emp);
        controller.registarEquipamentos();
        controller.novoPerfil();
        // Preencher CB equipamentos
        for (Equipamento e : controller.getRe().getListaEquipamentos()) {
            cb_equipamentos.getItems().add(e.getEndLogico());
        }
        //cb_equipamentos.getItems().add(new Equipamento("Equipamento 1"));

        //Preencher CB Dia da Semana
        cb_DiaSemana.getItems().add("Segunda-Feira");
        cb_DiaSemana.getItems().add("Terça-Feira");
        cb_DiaSemana.getItems().add("Quarta-Feira");
        cb_DiaSemana.getItems().add("Quinta-Feira");
        cb_DiaSemana.getItems().add("Sexta-Feira");
        cb_DiaSemana.getItems().add("Sábado");
        cb_DiaSemana.getItems().add("Domingo");

    }

    @FXML
    private void handleCbEquipamentos(ActionEvent event) {
    }

    @FXML
    private void adicionarLista(ActionEvent event) {
        alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("ERRO");
        try {
            Hora horaInicio = new Hora(Integer.parseInt(txf_horaInicio.getText()), Integer.parseInt(txf_MinutosInicio.getText()));
            Hora horaFim = new Hora(Integer.parseInt(txf_HorasFim.getText()), Integer.parseInt(txf_MinutosFIm.getText()));
            if (!(horaInicio.horasValidas() && horaFim.horasValidas())) {
                alerta.setHeaderText("Horas Inválidas.");
                alerta.showAndWait();
            } else if (horaFim.isMenor(horaInicio)) {
                alerta.setHeaderText("A hora de fim é menor que a hora de início.");
                alerta.showAndWait();
            } else if (cb_DiaSemana.getSelectionModel().getSelectedIndex() == -1) {
                alerta.setHeaderText("Selecione um dia da semana.");
                alerta.showAndWait();
            } else if (cb_equipamentos.getSelectionModel().getSelectedIndex() == -1) {
                alerta.setHeaderText("Selecione um equipamento.");
                alerta.showAndWait();
            } else if (txf_id.getText().isEmpty()) {
                alerta.setHeaderText("Introduza um ID.");
                alerta.showAndWait();
            } else {
                String e = cb_equipamentos.getValue();
                Periodo periodo = new Periodo(cb_DiaSemana.getValue(), horaInicio, horaFim, new Equipamento());
                listaPeriodos.getItems().add(periodo);

                cb_DiaSemana.getSelectionModel().select(-1);
                cb_equipamentos.getSelectionModel().select(-1);

                txf_MinutosFIm.setText("");
                txf_HorasFim.setText("");
                txf_horaInicio.setText("");
                txf_MinutosInicio.setText("");
            }

        } catch (NumberFormatException e) {
            alerta.setHeaderText("Horas inválidas. Insira apenas valores numéricos");
            alerta.showAndWait();
        }

    }

    @FXML
    private void handleDiaSemana(ActionEvent event) {

    }

    @FXML
    private void apagarLista(ActionEvent event) {
        listaPeriodos.getItems().remove(listaPeriodos.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void registar(ActionEvent event) throws IOException {

        controller.setDados(txf_id.getText(), txf_descricao.getText());

        for (Periodo periodo : listaPeriodos.getItems()) {
            controller.addPeriodo(periodo);

        }

        if (controller.registaPerfil()) {
            alerta = new Alert(Alert.AlertType.INFORMATION);
            alerta.setTitle("Registar Perfil");
            alerta.setHeaderText("Registo concluído com sucesso");
            alerta.showAndWait();

            Stage stage = (Stage) btt_cancelar.getScene().getWindow();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            stage.setTitle("Menu");
            stage.setScene(scene);
            stage.show();
        } else {
            alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Registar Perfil");
            alerta.setHeaderText("Registo não concluído com sucesso");
            alerta.showAndWait();
        }
    }

    @FXML
    private void cancelar(ActionEvent event) throws IOException {

        alerta = new Alert(Alert.AlertType.CONFIRMATION);
        alerta.setTitle("Deseja cancelar?");

        if (alerta.showAndWait().get() != ButtonType.CANCEL) {
            Stage stage = (Stage) btt_cancelar.getScene().getWindow();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            stage.setTitle("Menu");
            stage.setScene(scene);
            stage.show();
        }
    }

}
