/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.UI;

import com.mycompany.Modelo.Empresa;
import com.mycompany.pprog_1dk_1170606_1170570.MainApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Utilizador
 */
public class MenuPrincipal implements Initializable {

    @FXML
    private Button btnCancela;
    @FXML
    private Button btnRegisto;
    @FXML
    private Button brnAcesso;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
//    public void associarEmpresa(Empresa emp) {
//        this.empresa=emp;
//    }
    
    @FXML
    private void salir(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnCancela.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void novaJanela(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnRegisto.getScene().getWindow();
            
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/RegistoPerfil.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("Registo de Perfis");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void acederAR(ActionEvent event) throws IOException {
        Stage stage = (Stage) brnAcesso.getScene().getWindow();
            
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/AcessoAR.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("Acesso Área Restrita");
        stage.setScene(scene);
        stage.show();
    }
    
}
