/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.UI;

import Controller.AcederARController;
import Utils.Data;
import Utils.Hora;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Utilizador
 */
public class AcessoAR implements Initializable {

    private Alert alerta;
    @FXML
    private TextField txf_idCartao;
    @FXML
    private TextField txf_idEquipamento;
    @FXML
    private Button bttAceder;
    @FXML
    private Button bttCancela;
    private AcederARController controller = new AcederARController();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void acederAR(ActionEvent event) throws IOException {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dia = cal.get(Calendar.DAY_OF_MONTH);
        int mes = cal.get(Calendar.MONTH);
        int ano = cal.get(Calendar.YEAR);
        int horas = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        controller.solicitaAcesso(txf_idEquipamento.getSelectedText(), txf_idCartao.getSelectedText(), new Data(ano, mes, dia), new Hora(horas, min));
        Stage stage = (Stage) bttCancela.getScene().getWindow();

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("Menu");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void cancelaAcao(ActionEvent event) throws IOException {

        alerta = new Alert(Alert.AlertType.CONFIRMATION);
        alerta.setTitle("Deseja cancelar?");

        if (alerta.showAndWait().get() != ButtonType.CANCEL) {
            Stage stage = (Stage) bttCancela.getScene().getWindow();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            stage.setTitle("Menu");
            stage.setScene(scene);
            stage.show();
        }
    }
}
