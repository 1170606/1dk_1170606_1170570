package com.mycompany.pprog_1dk_1170606_1170570;

import Utils.SavingBin;
import com.mycompany.Modelo.Empresa;
import com.mycompany.Modelo.Equipamento;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainApp extends Application {
    private Empresa emp; 

    @Override
    public void start(Stage stage) throws Exception {
        
        emp = SavingBin.ler();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MenuPrincipal.fxml"));
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("Menu");
        stage.setScene(scene);
        stage.show();
        
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public Empresa getEmp() {
        return emp;
    }
    
    
    
}
