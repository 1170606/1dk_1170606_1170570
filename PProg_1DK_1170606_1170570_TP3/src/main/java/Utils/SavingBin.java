/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import com.mycompany.Modelo.Empresa;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Utilizador
 */
public class SavingBin {

    public static final String NOME_FICHEIRO = "dados.bin.bin";


    public static Empresa ler() {
        return ler(new File(NOME_FICHEIRO));
    }

    public static Empresa ler(String nomeFicheiro) {
        return ler(new File(nomeFicheiro));
    }

    public static Empresa ler(File ficheiro) {
        Empresa listaAcessos;
        try {
            try (ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(ficheiro))) {
                listaAcessos = (Empresa) in.readObject();
            }
            return listaAcessos;
        } catch (IOException | ClassNotFoundException ex) {
            return new Empresa();
        }
    }

    public static boolean guardar(Empresa lista) {
        return guardar(new File(NOME_FICHEIRO), lista);
    }

    public static boolean guardar(String nomeFicheiro, Empresa lista) {
        return guardar(new File(nomeFicheiro), lista);
    }

    public static boolean guardar(File ficheiro, Empresa lista) {
        try {
            try (ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(ficheiro))) {
                out.writeObject(lista);
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
