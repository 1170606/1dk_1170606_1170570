/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Utilizador
 */
public class Hora {

    private int horas;
    private int minutos;

    public Hora(int horas, int minutos) {
        this.horas = horas;
        this.minutos = minutos;
    }

    public int getHoras() {
        return horas;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d", horas, minutos);
    }

    public boolean isMaior(Hora outraHora) {
        int totalDias = contarMinutos();
        int totalDias1 = outraHora.contarMinutos();

        return totalDias > totalDias1;
    }

    public boolean isMenor(Hora outraHora) {
        int totalDias = contarMinutos();
        int totalDias1 = outraHora.contarMinutos();

        return totalDias < totalDias1;
    }

    private int contarMinutos() {
        return horas * 60 + minutos;
    }
    
    public boolean horasValidas(){
        return (this.horas >= 0 && this.horas <= 23 && this.minutos >= 0 && this.minutos < 59);
        
    }
    
}