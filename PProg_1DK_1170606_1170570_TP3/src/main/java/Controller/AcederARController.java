package Controller;

import Utils.Data;
import Utils.Hora;
import com.mycompany.Modelo.*;

public class AcederARController {

    Empresa emp;
    RegistoEquipamentos re;
    RegistoCartoes rc;
    RegistoAcessos ra;
    Equipamento e;
    Cartao c;
    AreaRestrita area;
    int mov;
    Colaborador colab;

    public AcederARController() {
    }

    public AcederARController(Empresa emp) {
        this.emp = emp;
    }

    /**
     *
     * @param IDEquipamento
     * @param IDCartao
     * @param data
     * @param hora
     */
    public boolean solicitaAcesso(String IDEquipamento, String IDCartao, Data data, Hora hora) {
        emp = new Empresa();
        re = emp.getRegistoEquipamentos();
        //e = re.getEquipamentoByID(IDEquipamento);
        e = new Equipamento();
        rc = emp.getRc();
        //c = rc.getCartao(IDCartao);
        c = new Cartao(IDCartao);
        //colab = c.getColaboradorAtribuido(data);
        colab = new Colaborador(1, "q", "q");
//        if (!colab.getPerfil().isEquipamentoAutorizado(e, data, hora)) {
//            return false;
//        }
        area = e.getAreaRestrita();
        mov = e.getMovimento();
//        if (!area.lotacaoPermiteAcesso(mov)) {
//            return false;
//        }
        ra = emp.getRegistoAcessos();
        //ra.novoAcesso(e, c, colab, data, hora, true, mov);
        return true;
    }

}
