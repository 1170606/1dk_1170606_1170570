package Controller;

import com.mycompany.Modelo.AreaRestrita;
import com.mycompany.Modelo.Empresa;
import com.mycompany.Modelo.Equipamento;
import com.mycompany.Modelo.PerfilA;
import com.mycompany.Modelo.Periodo;
import com.mycompany.Modelo.RegistoEquipamentos;
import com.mycompany.Modelo.RegistoPerfilA;
import java.io.Serializable;
import java.util.ArrayList;

public class RegistarPerfilController implements Serializable {

    PerfilA perfil;
    Empresa emp;
    RegistoPerfilA rp;
    RegistoEquipamentos re;
    ArrayList<Periodo> periodos;

    public RegistarPerfilController(Empresa emp) {
        this.emp = emp;
        periodos = new ArrayList<>();
    }

    public void novoPerfil() {
        rp = emp.getRegistoPerfil();
        periodos = new ArrayList<>();
        perfil = rp.novoPerfilDeAutorizacao();
    }

    /**
     *
     * @param id
     * @param descricao
     */
    public void setDados(String id, String descricao) {
        perfil.setId(id);
        perfil.setDescricao(descricao);
        re = emp.getRegistoEquipamentos();
        re.getListaEquipamentos();
    }

    private void setEmpresa(Empresa emp) {
        this.emp = emp;
    }

    /**
     *
     * @param periodo
     */
    public void addPeriodo(Periodo periodo) {
        periodos.add(periodo);
    }

    public boolean registaPerfil() {
        perfil.setPeriodos(periodos);
        return emp.getRegistoPerfil().registaPerfil(perfil);
    }

    public RegistoEquipamentos getRe() {
        return re;
    }
    
    
    public void registarEquipamentos(){
       re = new RegistoEquipamentos();
       Equipamento e1 = new Equipamento("1","12",3, new AreaRestrita(0));
       re.addEquipamentos(e1);
       Equipamento e2 = new Equipamento("2","13",24, new AreaRestrita(20));
       re.addEquipamentos(e2);
       Equipamento e3 = new Equipamento("3","14",5,new AreaRestrita(6));
       re.addEquipamentos(e3);
       
    }
    
    
}
